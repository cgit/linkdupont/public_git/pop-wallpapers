%define		debug_package %{nil}

Name:		system76-wallpapers
Version:	17.04.0
Release:	1%{?dist}
Summary:	A collection of System76 Wallpapers

License:	GPLv2
URL:		https://launchpad.net/system76-wallpapers
Source0:	https://launchpad.net/~system76-dev/+archive/stable/+files/%{name}_%{version}.tar.gz

BuildArch:	noarch

%description
A collection of System76 Wallpapers


%prep
%autosetup -n %{name}


%build



%install
install -m755 -d %{buildroot}%{_datadir}/backgrounds
install -m644 -D backgrounds/* %{buildroot}%{_datadir}/backgrounds/
install -m644 -D %{name}.xml %{buildroot}%{_datadir}/gnome-background-properties/%{name}.xml


%files
%doc
%{_datadir}/backgrounds/*
%{_datadir}/gnome-background-properties/%{name}.xml


%changelog
* Fri Apr 7 2017 Link Dupont - 17.04.0-1
- New upstream release

* Thu Sep 22 2016 Link Dupont - 16.10.0-1
- New upstream release

* Fri Apr 22 2016 Link Dupont - 16.04.1-1
- New upstream release

* Sun Jan 10 2016 Link Dupont - 15.10.0-3
- Update description

* Sat Jan 9 2016 Link Dupont - 15.10.0-2
- Remove invalid pre/post scriptlets

* Tue Jan 5 2016 Link Dupont - 15.10.0-1
- Initial package
